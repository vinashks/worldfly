package com.worldply.merch.controller;

import com.worldply.merch.modal.Offer;
import com.worldply.merch.sevice.IOfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping (value = "/api/offer")
public class OfferController {
    @Autowired
    IOfferService offerService;

    @PostMapping
    public Offer add (@RequestBody Offer offer) {
        return offerService.add(offer);
    }

    @GetMapping
    public Offer get(@RequestParam(value = "id") Long id) {
        return offerService.get(id);
    }

    @GetMapping (value = "/open")
    public List<Offer> getAllOpenOffers () {
        return offerService.getAllOpenOffers();
    }

    @PutMapping
    public void stopOffer ( @RequestBody Offer offer) {
        offerService.stop(offer.getId());
    }
}
