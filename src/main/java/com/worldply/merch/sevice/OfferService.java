package com.worldply.merch.sevice;

import com.worldply.merch.db.modal.DAOOffer;
import com.worldply.merch.db.modal.DAOProduct;
import com.worldply.merch.db.repository.OfferRepository;
import com.worldply.merch.modal.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OfferService implements IOfferService {
    @Autowired
    OfferRepository offerRepository;

    @Autowired
    ProductService productService;

    @Transactional
    public Offer add (Offer offer) {
        if (validateOffer(offer)) {
            DAOOffer daoOffer = new DAOOffer(offer);
            offerRepository.saveAndFlush(daoOffer);

            offer.setId(daoOffer.getId());

            return offer;
        } else {
            return null;
        }
    }

    public Offer get (Long id) {
        if (offerRepository.findById(id).isPresent()) {
            DAOOffer daoOffer = offerRepository.findById(id).get();
            Offer offer = new Offer(daoOffer);
            return offer;
        }
        return null;
    }

    @Transactional
    public List<Offer> getAllOpenOffers () {
        List<Offer> offerList = new ArrayList<>();
        List<DAOOffer> daoOfferList = offerRepository.getAllByEndTimeAfter(LocalDateTime.now());

        daoOfferList.forEach(daoOffer -> {
            Offer offer = new Offer(daoOffer);
            offerList.add(offer);
        });
        return offerList;
    }

    public void stop (Long id) {
        if (offerRepository.findById(id).isPresent()) {
            DAOOffer daoOffer = offerRepository.findById(id).get();
            daoOffer.setEndTime(LocalDateTime.now());

            offerRepository.saveAndFlush(daoOffer);
        }
    }

    public void delete (Long id) {
        if (offerRepository.findById(id).isPresent()) {
            DAOOffer daoOffer = offerRepository.findById(id).get();
            offerRepository.delete(daoOffer);
        }
    }

    private Boolean validateOffer (Offer offer) {
        Boolean valid = productService.isValidProduct(offer.getProductId());

        if (valid && (offer.getActualPrice() < offer.getOfferPrice())) {
            valid = false;
        }

        return valid;
    }
}
