package com.worldply.merch.sevice;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.worldply.merch.db.modal.DAOProduct;
import com.worldply.merch.db.repository.ProductRepository;
import com.worldply.merch.modal.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService implements IProductService{
    @Autowired
    ProductRepository productRepository;

    public Product add (Product product) {
        DAOProduct daoProduct = new DAOProduct(product);
        productRepository.saveAndFlush(daoProduct);
        product.setId(daoProduct.getId());
        return product;
    }

    public Product get (Long id) {
        if(productRepository.findById(id).isPresent()) {
            DAOProduct daoProduct = productRepository.findById(id).get();
            Product product = new Product(daoProduct);
            return product;
        }

        return null;
    }

    public Boolean isValidProduct (Long id) {
        return productRepository.findById(id).isPresent();
    }

    public void update (Product offer) {

    }

    public void delete (Long id) {
        if(productRepository.findById(id).isPresent()) {
            productRepository.delete(productRepository.findById(id).get());
        }
    }
}
