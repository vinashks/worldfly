package com.worldply.merch.sevice;

import com.worldply.merch.modal.Offer;

import java.util.List;

public interface IOfferService {
    public Offer add (Offer offer);

    public Offer get (Long id) ;

    public List<Offer> getAllOpenOffers ();

    public void stop (Long id);

    public void delete (Long id);
}
