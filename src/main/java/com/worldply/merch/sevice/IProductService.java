package com.worldply.merch.sevice;

import com.worldply.merch.modal.Product;

public interface IProductService {
    public Product add(Product product);

    public Product get(Long id);

    public Boolean isValidProduct(Long id);

    public void update(Product offer);

    public void delete(Long id);

}
