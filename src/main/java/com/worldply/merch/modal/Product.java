package com.worldply.merch.modal;

import com.worldply.merch.db.modal.DAOProduct;

public class Product {
    Long id;
    String name;

    public Product () {}

    public Product (DAOProduct daoProduct) {
        this.id = daoProduct.getId();
        this.name = daoProduct.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
