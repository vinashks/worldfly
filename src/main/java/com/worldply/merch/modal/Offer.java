package com.worldply.merch.modal;

import com.worldply.merch.db.modal.DAOOffer;

import java.time.LocalDateTime;

public class Offer {
    Long id;
    Long productId;
    LocalDateTime startTime;
    LocalDateTime endTime;
    Double offerPrice;
    Double actualPrice;
    String currency;
    Boolean valid;

    public Offer () {}

    public Offer (DAOOffer daoOffer) {
        this.id = daoOffer.getId ();
        this.productId = daoOffer.getProductId();
        this.startTime = daoOffer.getStartTime();
        this.endTime = daoOffer.getEndTime();
        this.offerPrice = daoOffer.getOfferPrice();
        this.actualPrice = daoOffer.getActualPrice();
        this.currency = daoOffer.getCurrency();
        this.valid = daoOffer.getEndTime().isAfter(LocalDateTime.now());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
