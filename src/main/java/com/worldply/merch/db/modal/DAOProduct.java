package com.worldply.merch.db.modal;

import com.worldply.merch.modal.Product;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class DAOProduct {
    @Id
    @GeneratedValue
    @Column (name = "id")
    Long id;

    @Column (name = "name")
    String name;

    public DAOProduct () {}

    public DAOProduct (Product product) {
        this.id = product.getId();
        this.name = product.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
