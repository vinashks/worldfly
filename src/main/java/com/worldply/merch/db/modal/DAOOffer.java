package com.worldply.merch.db.modal;

import com.worldply.merch.modal.Offer;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table (name = "offer")
public class DAOOffer {
    @Id
    @GeneratedValue
    @Column (name = "id")
    Long id;

    @Column (name = "start_time")
    LocalDateTime startTime;

    @Column (name = "end_time")
    LocalDateTime endTime;

    @Column (name = "product_id")
    Long productId;

    @Column (name = "offer_price")
    Double offerPrice;

    @Column (name = "actual_price")
    Double actualPrice;

    @Column (name = "currency")
    String currency;

    public DAOOffer () {}

    public DAOOffer (Offer offer) {
        this.id = offer.getId ();
        this.productId = offer.getProductId();
        this.startTime = offer.getStartTime();
        this.endTime = offer.getEndTime();
        this.offerPrice = offer.getOfferPrice();
        this.actualPrice = offer.getActualPrice();
        this.currency = offer.getCurrency();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
