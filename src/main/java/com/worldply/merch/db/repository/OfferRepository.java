package com.worldply.merch.db.repository;

import com.worldply.merch.db.modal.DAOOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public interface OfferRepository extends JpaRepository <DAOOffer, Long> {
    public List<DAOOffer> getAllByEndTimeAfter(LocalDateTime currentTime);
}
