package com.worldply.merch.db.repository;

import com.worldply.merch.db.modal.DAOProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ProductRepository extends JpaRepository<DAOProduct, Long> {
}
