package com.worldply.merch.Controller;

import com.worldply.merch.controller.OfferController;
import com.worldply.merch.modal.Offer;
import com.worldply.merch.modal.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class OfferControllerTest {
    MockMvc mockMvc;

    @Mock
    private OfferController offerController;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(offerController).build();
    }

    Long productId;

    @Test
    public void testAddOffer () {
        if (productId == null) {
            addProduct();
        }

        Offer offer = new Offer();
        offer.setActualPrice(10.0);
        offer.setCurrency("pound");
        offer.setOfferPrice(8.0);
        offer.setProductId(productId);

        LocalDateTime startTime =  LocalDateTime.of(2018, Month.JANUARY, 1, 1, 10, 5);
        offer.setStartTime(startTime);

        LocalDateTime endTime =  LocalDateTime.of(2019, Month.NOVEMBER, 30, 10, 10, 5);
        offer.setEndTime(endTime);

        ResponseEntity<Offer> response = template.postForEntity("/api/offer", offer, Offer.class);
        offer = response.getBody();

        Assert.assertEquals(200, response.getStatusCode().value());
        Assert.assertNotNull(offer.getId());
    }

    @Test
    public void testGetOpenOffers () {
        ResponseEntity<List> response = template.getForEntity("/api/offer/open", List.class);
        Assert.assertEquals(200,response.getStatusCode().value());
    }

    @Test
    public void testStopOffer () {
        if (productId == null) {
            addProduct();
        }

        ResponseEntity<List> response = template.getForEntity("/api/offer/open", List.class);
        Integer initialSize = response.getBody().size();

        Offer offer = new Offer();
        offer.setActualPrice(10.0);
        offer.setCurrency("pound");
        offer.setOfferPrice(8.0);
        offer.setProductId(productId);

        LocalDateTime startTime =  LocalDateTime.of(2018, Month.JANUARY, 1, 1, 10, 5);
        offer.setStartTime(startTime);

        LocalDateTime endTime =  LocalDateTime.of(2019, Month.NOVEMBER, 30, 10, 10, 5);
        offer.setEndTime(endTime);

        ResponseEntity<Offer> addResponse = template.postForEntity("/api/offer", offer, Offer.class);
        offer = addResponse.getBody();

        Assert.assertEquals(200, addResponse.getStatusCode().value());
        Assert.assertNotNull(offer.getId());

        template.getForEntity("/api/offer/open", List.class);
        Integer sizeAfterAdd = response.getBody().size();

        template.put("/api/offer", offer,  "id="+offer.getId());

        template.getForEntity("/api/offer/open", List.class);
        Integer sizeAfterStop = response.getBody().size();

        Assert.assertEquals(initialSize, sizeAfterStop);
    }

    @Test
    public void testGetOffer () {
        if (productId == null) {
            addProduct();
        }

        Offer offer = new Offer();
        offer.setActualPrice(100.0);
        offer.setCurrency("pound");
        offer.setOfferPrice(50.0);
        offer.setProductId(productId);

        LocalDateTime startTime = LocalDateTime.of(2018, Month.JANUARY, 1, 1, 10, 5);
        offer.setStartTime(startTime);

        LocalDateTime endTime = LocalDateTime.of(2019, Month.SEPTEMBER, 30, 10, 10, 5);
        offer.setEndTime(endTime);

        ResponseEntity<Offer> addResponse = template.postForEntity("/api/offer", offer, Offer.class);
        offer = addResponse.getBody();

        Assert.assertEquals(200, addResponse.getStatusCode().value());
        Assert.assertNotNull(offer.getId());

        ResponseEntity<Offer> getResponse = template.getForEntity("/api/offer?id=" + offer.getId(), Offer.class);
        offer = getResponse.getBody();

        Assert.assertEquals(200, addResponse.getStatusCode().value());
        Assert.assertNotNull(offer.getId());
    }

    public void  addProduct () {
        Product product = new Product();
        product.setName("Product 1");

        ResponseEntity<Product> response = template.postForEntity("/api/product", product, Product.class);
        product = response.getBody();

        productId = product.getId();
    }
}
