package com.worldply.merch.Controller;

import com.worldply.merch.controller.ProductController;
import com.worldply.merch.modal.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProductControllerTest {

    MockMvc mockMvc;

    @Mock
    private ProductController productController;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    public void testAddProduct () {
        Product product = new Product();
        product.setName("Book");

        ResponseEntity<Product> response = template.postForEntity("/api/product", product, Product.class);
        product = response.getBody();

        Assert.assertEquals(200, response.getStatusCode().value());
        Assert.assertNotNull(product.getId());
    }
}
